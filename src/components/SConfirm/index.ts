import { createVNode, render } from 'vue'
import Confirm from './index.vue'

const div = document.createElement('div')
document.body.appendChild(div)

export default (text:string,options?:{title?:string,type?:string}) => {
  return new Promise((resolve, reject) => {

    // 点击确定按钮
    const submitCallback = () => {
      render(null, div)
      resolve()
    }
    // 点击取消按钮
    const cancelCallback = () => {
      render(null, div)
      reject('点击取消')
    }

    const vnode = createVNode(Confirm, { text, submitCallback, cancelCallback, ...options })
    render(vnode, div)
  })
}
