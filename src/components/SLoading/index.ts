import { createVNode, render } from 'vue'
// 引入 loading 组件
import Loading from './index.vue'

const vLoding = {
    mounted: (el, binding) => {
        if (binding.value) {
            const vm = createVNode(Loading)
            el.style.position = 'relative'
            render(vm, el)
        }
    },
    updated: (el, binding) => {
        const vm = createVNode(Loading)
        if (binding.value !== binding.oldValue) {
            binding.value ? render(vm, el) : render(null, el)
        }
    }
}
export default vLoding

