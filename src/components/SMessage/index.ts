import { MESSAGE_TIMEOUT } from "@/utils";
import { createVNode, render } from "vue";
import SMessage from "./SMessage.vue";

const div = document.createElement("div");
// 添加到body上
document.body.appendChild(div);

// 定时器标识
let timer: any = null;

const renderMessage = (vnode: any, timeout: number) => {
   render(null, div);
   render(vnode, div);
   clearTimeout(timer);
   timer = setTimeout(() => {
      render(null, div);
   }, timeout);
};

export default {
   error: (text: string, timeout: number = MESSAGE_TIMEOUT) => {
      const vnode = createVNode(SMessage, { type: "error", text, timeout });
      renderMessage(vnode, timeout);
   },
   warn: (text: string, timeout: number = MESSAGE_TIMEOUT) => {
      const vnode = createVNode(SMessage, { type: "warn", text, timeout });
      renderMessage(vnode, timeout);
   },
   success: (text: string, timeout: number = MESSAGE_TIMEOUT) => {
      const vnode = createVNode(SMessage, { type: "success", text, timeout });
      renderMessage(vnode, timeout);
   },
   info: (text: string, timeout: number = MESSAGE_TIMEOUT) => {
      const vnode = createVNode(SMessage, { type: "info", text, timeout });
      renderMessage(vnode, timeout);
   },
};
