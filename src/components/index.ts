import SButton from '@/components/SButton.vue'
import SInput from '@/components/SInput.vue'
import SIcon from '@/components/SIcon.vue'
import SCheckbox from '@/components/SCheckbox.vue'
import SFormItem from './SFormItem.vue'
import SForm from './SForm.vue'
import SRadioGroup from './SRadioGroup.vue'
import STable from './STable.vue'
import STableColumn from './STableColumn.vue'
import SCard from './SCard.vue'
import SUpload from './SUpload.vue'
import SModal from './SModal.vue'
import SProgress from './SProgress.vue'
import SBadge from './SBadge.vue'
import SSelect from './SSelect.vue'
import Message from '@/components/SMessage/index'
import Confirm from '@/components/SConfirm/index'
import Loading from '@/components/SLoading/index'
import SCheckboxGroup from './SCheckboxGroup.vue'




export {
    SCheckboxGroup,SSelect,SButton, SInput, SIcon, Message, SCheckbox, SFormItem, SForm, SRadioGroup, STable, SCard, STableColumn, SUpload, Confirm, Loading, SModal, SProgress, SBadge
}