import {
   computed,
   reactive,
   toRefs
} from "vue";

import { REBOOT_TIME,Timer } from "@/utils"
import { useUser } from "@/hooks"

export const useLoad = () => {
   const { logout } = useUser()
   enum STATUS {
      FREE = 0, // 空状态

      UPDATE_UPLOADING = 1, // 上传文件
      UPDATE_START = 2, // 升级
      UPDATE_REBOOTING = 3, // 重启
      UPDATE_ERROR = 4, // 错误

      DEFAULT_START = 5, // 开始执行恢复默认
      DEFAULT_REBOOTING = 6, // 成功恢复 重启
      DEFAULT_ERROR = 7, //

      REBOOT_START = 8, //
      REBOOT_ING = 9,
      REBOOT_END = 10, //
      REBOOT_ERROR = 11, //

      CONFIG_UPLOADING = 12, // 配置文件上传
      CONFIG_ERROR = 13, //
      CONFIG_REBOOTING = 14, //
   }

   const state = reactive({
      loadStatus: STATUS.FREE,
      loadVal: 0,
   });

   const statusMap = {
      [STATUS.FREE]: { tip: "提示", progress: "" ,showClose: true },
      [STATUS.UPDATE_UPLOADING]: { tip: "正在上传文件中...请不要断电！", progress: "loading",showClose:false },
      [STATUS.UPDATE_START]: { tip: "正在升级...", progress: "step" ,showClose:false },
      [STATUS.UPDATE_REBOOTING]: { tip: "设备正在重启中...请稍后", progress: "step",showClose:false },
      [STATUS.UPDATE_ERROR]: { tip: "升级出错，请重试！", progress: "error" ,showClose:true},
      [STATUS.DEFAULT_START]:{ tip: "正在恢复出厂设置，请稍等...", progress: "loading" ,showClose:false},
      [STATUS.DEFAULT_REBOOTING]:{tip: "恢复出厂设置成功，系统自动重启中...!", progress: "loading" ,showClose:false},
      [STATUS.DEFAULT_ERROR]:{tip: "恢复出厂设置出错,请重试！",progress:"error" ,showClose:true},
      [STATUS.REBOOT_START]: {tip: "正在处理重启请求...",progress:"loading" ,showClose:false},
      [STATUS.REBOOT_ING]: {tip: "系统重启中...",progress:"loading", showClose:false},
      [STATUS.REBOOT_END]: {tip: "系统重启成功",progress:"", showClose:true},
      [STATUS.REBOOT_ERROR]: {tip: "系统重启失败，请重试",progress:"error",showClose:true},
      [STATUS.CONFIG_UPLOADING]: {tip: "正在上传文件中...请不要断电！",progress:"loading",showClose:false},
      [STATUS.CONFIG_ERROR]: {tip: "导入失败",progress:"error",showClose:true},
      [STATUS.CONFIG_REBOOTING]: {tip: "导入成功，系统自动重启中...!",progress:"loading",showClose:false}
   }


   // 显示关闭的情况
   const showClose = computed(() => {
      return statusMap[state.loadStatus].showClose;
   })


   const progressType = computed(() => {
      return statusMap[state.loadStatus].progress;
   })

   const tip = computed(() => {
      return statusMap[state.loadStatus].tip;
   });


   const loadShow = computed(() => {
      return state.loadStatus !== STATUS.FREE
   })

   const changeStatus = (value: number) => {
      state.loadStatus = value
   }

   const closeLoad = () => {
      state.loadStatus = STATUS.FREE
      state.loadVal = 0
   }

   const changeLoadVal = (val: number) => {
      state.loadVal = val
   }

   const goReboot = () => {
      clearInterval(Timer.refreshSysInfo)
      let timer: any = null
      timer = setTimeout(() => {
         closeLoad()
         logout()
         clearTimeout(timer)
      }, REBOOT_TIME)
   }

   return {
      ...toRefs(state),
      tip,
      STATUS,
      loadShow,
      changeStatus,
      closeLoad,
      changeLoadVal,
      REBOOT_TIME,
      goReboot,
      showClose,
      progressType
   };
};
