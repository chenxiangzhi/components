import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "@/assets/styles/index.less";
import "@/assets/icons/index.js"

import {Loading} from "@/components"
const app=createApp(App)
app.directive('loading',Loading)
app.use(router).mount("#app");
