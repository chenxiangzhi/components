import $axios from './config';
import { METHOD } from '@/utils';

// 获取参数列表
export function getData() {
    const url = 'getData';
    return $axios[METHOD](url);
};

export function goLogin(){
    const url = 'Login';
    return $axios[METHOD](url);
}

export function getTime(){
    const url = 'GetTime';
    return $axios[METHOD](url);
}

export function setTime(data:any){
    const url = 'SetTime';
    return $axios[METHOD](url,data);
}
