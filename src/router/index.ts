import { createRouter, createWebHashHistory } from "vue-router";
import { routes } from "@/router/routes";

const router = createRouter({
   history: createWebHashHistory(),
   routes,
});

router.beforeEach((to,from,next)=>{
   if(to.path!=="/login"){
       if(localStorage.getItem("token")){
           return next();
       }else{
          return next("/login") 
       }
   }
   return next();
})


export default router;
