import Login from "@/views/LoginView.vue";
import HomeView from "@/views/HomeView.vue"
import CommomView from "@/views/CommomView.vue"
import FormView from "@/views/FormView.vue"
import TableView from "@/views/TableView.vue"



import { MenuList } from "@types";
const menuList: MenuList = [
   {
      path: "/home",
      component: HomeView,
      title: "首页",
      key: "home",
      name: "home"
   },
   {
      path: "/commom",
      component: CommomView,
      title: "通用",
      key: "commom",
      name: "commom"
   },
   {
      path: "/form",
      component: FormView,
      title: "表单",
      key: "form",
      name: "form"
   },
   {
      path: "/table",
      component: TableView,
      title: "表格",
      key: "table",
      name: "table"
   }
];

const routes = [
   { path: "/login", component: Login, name: "login" },
   {
      path: "/",
      redirect:"/home",
      component: () => import("@/layout/LayoutIndex.vue"),
      children: menuList,
   },
];

export { routes, menuList };
