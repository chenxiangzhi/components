// 下载
export const download=(url:string, fileName:string)=>{
    const aElement = document.createElement('a');
    document.body.appendChild(aElement);
    aElement.style.display = 'none';
    aElement.href = url;
    aElement.download = fileName || '下载';
    aElement.click();
    document.body.removeChild(aElement);
};

// 反向映射
export const getOptions=(data:Object)=>{
    let options=[]
    for (const [key, value] of Object.entries(data)) {
        options.push({
            label:value,
            value:!isNaN(Number(key))?Number(key):key
        })
    }
    return options
}