export const METHOD=process.env.TYPE==='dev'?'get':'post' // 调用方法
export const REBOOT_TIME = 50 * 1000;                     // 关机时长
export const MESSAGE_TIMEOUT: number = 3000;              // 消息时长


export const STATUS = {
   0: "异常",
   1: "已启用",
};
export const HERTZ = {
   1: "44.1",
   2: "48",
};

export const RATE = {
   2: "80",
   3: "96",
   4: "112",
   5: "128",
   6: "160",
   7: "192",
};

export const AUDIO_TYPE = {
   1: "MP2",
   2: "AAC",
   // 4: "AC3",
};

export const MOCK_INPUT = {
   0: "无信号",
   1: "已锁定",
};
