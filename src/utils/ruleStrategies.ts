// 校验策略
import { IRuleStrategies } from '@types'

// 判断是否为空
export const isRequired = (val: string): boolean => {
    return val !== ""
}

// 判断是否为整数
export const isInteger = (value: any) => {
    return Number.isInteger(Number(value))
}

// 判断是否为ip
export const isIp = (ip: string): boolean => {
    var rep = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
    return rep.test(ip.toString())
}

// 判断端口是否合法
export const isPort = (port: number): boolean => {
    if (!isInteger(port) || 65535 < port || port < 1) {
        return false;
    }
    return true;
}

// 判断音量范围及合法性
export const isVol = (value: number, max: number = 100, min: number = 0): boolean => {
    if (!isInteger(value) || value > max || value < min) {
        return false
    }
    return true
}

export const isMac = (value: string) => {
    let reg_name = /^(([A-Fa-f0-9]{2})|[A-Fa-f0-9])(:([A-Fa-f0-9]{2})|:[A-Fa-f0-9]){5}$/;
    return reg_name.test(value);
};

export const isPwd = (str: string) => {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d_\-\*]{8,32}$/.test(str); //8到32位（大小写字母，数字，下划线，减号，星号）至少1个大写字母，1个小写字母和1个数字
}

// 导出
export const ruleStrategies: IRuleStrategies = {
    "required": isRequired,
    "ip": isIp,
    "port": isPort,
    "vol": isVol,
    "mac": isMac
}