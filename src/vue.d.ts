import { AxiosRequestConfig } from "axios"

declare module "*.vue" {
   import type { DefineComponent } from "vue"
   const component: DefineComponent<{}, {}, any>
   export default component
}

declare module 'md5'

declare module 'axios' {
   interface AxiosInstance {
      (config: AxiosRequestConfig):Promise<any>
   }
}