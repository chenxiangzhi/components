export interface Menu {
   path: string;
   title?: string;
   name?: string;
   key: string;
   component?: any;
   children?: Array<Menu>
}
export type MenuList = Array<Menu>;

// 校验
// 校验类型
export type RuleKeys = "required" | "ip" | "port" | "vol" | "mac";
//  校验对象
export interface RuleProp {
   type: RuleKeys;
   message: string;
}
// 校验列表
export interface IRulesProp {
   [index: string]: Array<RuleProp>;
}
// 校验策略
export interface IRuleStrategies {
   [index in RuleKeys]: (...params: any[]) => boolean;
   [index: string]: (...params: any[]) => boolean;
}

// 消息样式
type MessageStyle = {
   icon: string;
   color: string;
   backgroundColor: string;
   borderColor: string;
};
type MessageStyleKeys = "info" | "success" | "warn" | "error"; // 支持的类型

// 消息属性
type MessageProps = {
   text: string;
   type: MessageStyleKeys;
   timeout?: number;
};

// 表单
type FormValue = string | boolean | number;
export type BaseForm = {
   name: string;
   value: string | number | boolean;
};
export interface FormIndex {
   [index: string]: FormValue;
}

// 单选框
type Radio = {
   label: string;
   value: string | number;
   key?: string;
};
export type RadioOptions = Array<Radio>;

// 表格
export type Column = {
   prop: string;
   label: string;
   width?: string | number;
};
export type TableCell = {
   id: number | string;
   [index: string]: any;
};

export type TableLayout = "horizon" | "column";
export type TableAlign = "left" | "center" | "right";
export interface HeaderStyle {
   width?: string;
   height?: string;
   textAlign?: TableAlign;
   display?: string;
   alignItems?: string;
   justifyContent?: string;
}
export interface RealHeaderStyle extends HeaderStyle {
   minWidth?: string;
   minHeight?: string;
   lineHeight?: string;
   flex?: number;
   [index: string]: string | number | undefined;
}


// 升级
export type Version = {
   current_version: string;
   file: File | null;
};
export type NetForm = {
   id: number
   ip: string;
   mask: string;
   gateway: string;
   [index: string]: string | number | boolean;
};
export type Password = {
   curPwd: string,
   newPwd: string,
   surePwd: string,
   [index: string]: string | number | boolean;
}
export enum RUNING {
   FREE = 0,
   UPLOADING = 1,
   UPDATING = 2,
   REBORTING = 3,
   ERROR = 4
}

export type IconTypes = "icon-close" | "icon-user" | "icon-quit" | "icon-file" | "icon-warm-fill" | "icon-success-fill" | "icon-warning-fill" | "icon-info-fill" | "icon-error-fill" | "icon-play-fill" | "icon-stop-fill" | "icon-arrow-up"  | "icon-arrow-down"


// 锁屏属性
export interface LockScreenProps{
   ukeyenable:boolean,
   lockinterval:number
}
// 话筒属性
export interface MicrophoneProps{
   mricoenable:boolean,
   timeout:number
}
// 平台属性
export interface PlatfromProps{
   lock:number,
   ipaddr:string,
   ipport:number
}
// 音量属性
export interface VoiceSettingProps{
   listen:number,
   broadcast:number
}