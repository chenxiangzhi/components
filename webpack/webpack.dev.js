const webpack = require("webpack")
module.exports = {
   mode: "development",
   devtool: "cheap-module-source-map",
   plugins: [
      new webpack.DefinePlugin({
         "process.env": JSON.stringify({
            BASE_URL: "/api",
            UPLOAD_URL: "/api",
            TYPE: "dev"
         })
      })
   ],
   devServer: {
      hot: true,
      port: 8080
   }
}
