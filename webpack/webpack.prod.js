const webpack = require("webpack")
module.exports = {
   mode: "production",
   devtool: "source-map", // 定位
   plugins: [
      new webpack.DefinePlugin({
         "process.env": JSON.stringify({
            BASE_URL: "/ajax",
            UPLOAD_URL: "/upload",
            TYPE: "prod"
         })
      })
   ]
}
