const webpack = require("webpack")
module.exports = {
   mode: "development",
   devtool: "cheap-module-source-map",
   plugins: [
      new webpack.DefinePlugin({
         "process.env": JSON.stringify({
            BASE_URL: "/ajax",
            UPLOAD_URL: "/upload",
            TYPE: "proxy"
         })
      })
   ],
   devServer: {
      hot: true,
      port: 8081,
      proxy: {
         "/": {
            target: "http://192.168.22.202/",
            secure: false,
            changeOrigin: true
         }
      }
   }
}
